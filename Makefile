up:
	docker-compose -f ./docker/docker-compose.yml up -d

down:
	docker-compose -f ./docker/docker-compose.yml down

setup:
	docker-compose -f ./docker/docker-compose.yml build

clean:
	docker-compose -f ./docker/docker-compose.yml rm
