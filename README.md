# Lookdown
Lookdown is the next version of the first project [Lookup](https://gitlab.com/M-Felly/feyll_lookup).
Both projects are self presenting websites. This improved version is more interactive and mobile friendly.
It is the current running version on my domain [matthiasfeyll.de](https://matthiasfeyll.de)

## Requirements
- Angular
- npm
- PHP (optional)

## Installation
For the first time hit ``npm install`` to install all dependencies. After that you are ready to go with ``ng serve``.


## Deployment



## Author
Matthias Feyll
